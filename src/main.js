import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import Axios from 'axios'
import BootstrapVue from 'bootstrap-vue'

import Scoreboard from './components/Scoreboard.vue'
import Checks from './components/Checks.vue'
import Toggles from "./components/Toggles.vue";
import Numbers from "./components/Numbers.vue";
import Config from "./components/Config.vue";

Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.use(VueSidebarMenu)

const axiosInstance = Axios.create({
  baseURL: '/admin-api',
  // baseURL: 'http://localhost/admin-api',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 5000,
  maxContentLength: 20000,
});
// noinspection JSUnusedGlobalSymbols
Vue.prototype.$http = axiosInstance;

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'Scoreboard',
      component: Scoreboard
    },
    {
      path: '/checks',
      name: 'Checks',
      component: Checks
    },
    {
      path: '/toggles',
      name: 'Toggles',
      component: Toggles
    },
    {
      path: '/numbers',
      name: 'Numbers',
      component: Numbers
    },
    {
      path: '/config',
      name: 'Config override',
      component: Config
    }
  ]
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
