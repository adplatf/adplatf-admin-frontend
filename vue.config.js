module.exports = {
	lintOnSave: false,
	publicPath: '',
	productionSourceMap: false,
	devServer: {
		disableHostCheck: true
	}
};
